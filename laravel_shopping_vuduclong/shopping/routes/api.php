<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'  ], function () {
    Route::post('register', [AuthController::class,'register'])->name('register');
    Route::post('login', [AuthController::class,'login']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', [AuthController::class,'logout'])->name('logout');
        Route::get('user', [AuthController::class,'me'])->name('me');
    });
});
Route::group(['prefix'  ], function () {
    Route::resource('category','CategoryController');
    Route::resource('tag','TagController');
    // Route::resource('comment','CommentController');
    Route::resource('product','ProductController');
    Route::resource('user','UserController');
});
