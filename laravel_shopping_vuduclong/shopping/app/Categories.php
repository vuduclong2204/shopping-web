<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'name',
        'slug'
    ];

     /**
     * tao moi quan he
     */
    public function blogs() 
    {
        return $this->hasMany(Blogs::class);
    }

     /**
     * tao moi quan he
     */
    public function products() 
    {
        return $this->hasMany(Products::class);
    }
}
