<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Products extends Model
{

    protected $table = 'products';

    protected $fillable = [
        'code',
        'name',
        'description',
        'image_url',
        'price',
        'slug',
        'category_id',
    ];

    /**
     * nghich dao quan he one to many
     */
    public function category()
    {
        return $this->belongsTo(Categories::class);
    }

    /**
     * quan he many to many
     */
    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'product_tag', 'product_id', 'tag_id');
    }

    /**
     * quan he many to many
     */
    public function notifications()
    {
        return $this->belongsToMany(Notifications::class ,'product_notification','product_id','notification_id');
    }

    /**
     * quan he many to many
     */
    public function order()
    {
        return $this->belongsToMany(Orders::class, 'order_detail', 'product_id', 'order_id');
    }
}
