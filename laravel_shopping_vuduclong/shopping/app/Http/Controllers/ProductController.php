<?php

namespace App\Http\Controllers;

use App\Products;
use App\Cart;
use App\Services\CategoryService;
use App\Services\ProductService;    
use App\Services\TagService;
use App\Services\UserService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    protected $productService, $tagService, $categoryService, $userService;
    /**
     * @param   \App\Services\CategoryService;
     *  @param   \App\Services\ProductService;
     * @param   \App\Services\TagService;
     * @param   \App\Services\UserService;
     */
    public function __construct(UserService $userService,TagService $tagService, CategoryService $categoryService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
        $this->tagService = $tagService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products =  Products::paginate(config('paginateconfig.paginate_page'));;
        $products->load('Category'); 

        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = $this->tagService->getAll();
        $categories = $this->categoryService->getAll(); 
        
        return response()->json($categories,$tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($data['name']);
        if($request->hasFile('image_url')){
            $image = $request->file('image_url');
            $filename = time() . $image->getClientOriginalName();
            $path = public_path('images/'.$filename);
            Image::make($image)->resize(120, 120)->save($path);
            $data['image_url'] = $filename;
            
         }
         $product = $this->productService->create($data); 
        // Sync data after created blog with tag id
        $syncTag = $this->productService->syncTagDataProduct($product, $data);

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Cache::get('userID:' . $id);
        $user = auth()->id();
        $user = $this->userService->view($id);
        //dd($user);
        return response()->json($user);
    }

    public function view()
    {
        $product = $this->productService->getAll();

        return response()->json($product);
    }

    /**
     * controller user view
    */
    public function getOurProduct()
    {
        $products = $this->productService->getAll();

        return response()->json($products);
    }

   


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = $this->categoryService->getAll();
        $tags = $this->tagService->getAll();
        $product = $this->productService->editProduct($id);
        
        return view('product.pageEditProduct',compact('product','categories','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($request->hasFile('image_url')){
            $image = $request->file('image_url');
            $filename = time() . $image->getClientOriginalName();
            $path = public_path('images/'.$filename);
            Image::make($image)->resize(120, 120)->save($path);
            $data['image_url'] = $filename;
           
         }     
        $product = $this->productService->update($id,$data);
        // Sync data after update blog with tag id
        $syncTag = $this->productService->syncTagDataProduct($product, $data); 
        if ($syncTag == true) {
            session()->flash('create','create sucess');
        } else {
            session()->flash('create','Error!!');
        }
   
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->productService->find($id);
        $product->delete();
        session()->flash('delete','delete success');
        
        return redirect()->route('product.index');
    }
}
