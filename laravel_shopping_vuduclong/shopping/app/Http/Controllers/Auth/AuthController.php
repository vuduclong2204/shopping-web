<?php

namespace App\Http\Controllers\Auth;

use App\Services\AuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(Request $request)
    {
        $user = $this->authService->register($request->all());
        
        if (!$user) {
            return response()->json([
                'data' => [
                    'mesled!',
                ],
            ]);
        }

        return response()->json([
            'data' => [
                'user' => $user,
                'message' => 'Register successfully!',
            ],
        ]);
    }

    public function login(Request $request)
    {
        $data = $request->all();
        if ( !auth()->attempt($data)) {
            return response()->json([
                'data' => [
                    'message' => 'Login faileddd!',
                ],
            ]);
        }

        return response()->json([
            'data' => [
                'token' => auth()->user()->createToken('My app')->accessToken,
                'user' => auth()->user(),
                'message' => 'Login successfully!',
            ],
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'data' => [
                'message' => 'Logout successfully!',
            ],
        ]);
    }

    public function me(Request $request)
    {
        return response()->json([
            'data' => [
                'user' => $request->user(),
            ],
        ]);
    }
}
