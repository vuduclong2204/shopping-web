<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository;
use App\User;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return User::class;
    }

    
    /**
     * Edit 
     * 
     * @param $id ID from DB
     * @return string
     */
    public function edit($id)
    {
        $user = $this->model->where('slug', $id)->first();

        return $user;
    }

    /**
     * View Proflie
     * 
     * @param $user User from DB
     * @param $id ID from DB
     */
    public function view($id)
    {
        $user = $this->model->where('id', $id)->first();

        return $user;
    }

    /**
     * FindOrFail = Id
     * 
     * @param $id 
     * @return string
     */
    public function findUser($id)
    {
        $product = $this->model->findOrFail($id);

        return $product;
    }


}
?>