<?php

namespace App\Repositories\User;

use App\Repositories\RepositoryInterface;

interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * Edit 
     * 
     * @param $id ID from DB
     * @return string
     */
    public function edit($id);

      /**
     * View Proflie
     * 
     * @param $user User from DB
     * @param $id ID from DB
     */
    public function view($id);

    /**
     * FindOrFail = Id
     * 
     * @param $id 
     * @return string
     */
    public function findUser($id);
}
?>