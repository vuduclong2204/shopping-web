<?php


namespace App\Repositories\Product;

use App\Products;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Session;
use Laravel\Ui\Presets\React;
use Symfony\Component\HttpFoundation\Request;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return Products::class;
    }

    /**
     * Sync Data Tag
     * 
     * @param $product Product created
     * @param $data Data from request
     * @return Boolean
     */
    public function syncDataTag($product, $data)
    {
        try {
            $syncTag = $product->Tags()->sync($data['tag_id']);

            return true;
        } catch (\Throwable $th) {
           return false;
        }
        
    }

    /**
     * FindOrFail = Id
     * 
     * @param $id 
     * @return string
     */
    public function findID($id)
    {
        $product = $this->model->findOrFail($id);

        return $product;
    }

    /**
     * Edit Product
     * 
     * @param $id ID from DB
     * @return string
     */
    public function editProduct($id)
    {
        $product = $this->model->where('slug', $id)->first();

        return $product;
    }

    /**
     * Search Blog
     * 
     * @param $data Data from DB
     * @return array
     */
    public function searchProduct($data)
    {
        $products = $this->model->where('name', 'like', '%' . $data['name'] . '%' )
                                
                                ->paginate(config('paginateconfig.paginate_page'));

        return $products;
    }

     /**
     * View Proflie
     * 
     * @param $user User from DB
     * @param $id ID from DB
     */
    public function view($id)
    {
        $user = $this->model->where('id', $id)->first();

        return $user;
    }  

    /**
     * add to cart
     */
    public function addToCart($request)
    {
        $product = $this->model->find($request['product_id']);
        $cart = Session::get('cart') ?  Session::get('cart') : [] ;
        $key = array_search($product->id, array_column($cart, 'product_id'));
        if (count($cart) && $key !== false) {
           $cart[$key]['quantity']++;
        } else {
            $cart[] =  [
                'product_id' =>$product->id,
                'image_url' =>$product->image_url,
                'code' =>$product->code,
                'name' =>$product->name,
                'quantity' => 1,
                'price' => $product->price,
            ];

        }
        //dd($cart);
        Session::put('cart', $cart);
        
        return $cart;

    }
    /**
     * updat cart
     */
    public function updateCart($productId,Request $request)
    {
        $cart = $request->all();
        $cart = Session::get('cart') ?  Session::get('cart') : [] ;
        $key = array_search($productId, array_column($cart, 'product_id'));
        $cart[$key]['quantity'] = $request->quantity;
        
        Session::put('cart',$cart);
            session()->flash('success','Cart update successfully');
        return $cart;
        

    }

    public function removeCart($productId)
    {
       
        $cart = Session::get('cart') ?  Session::get('cart') : [] ;
        $key = array_search($productId, array_column($cart, 'product_id'));
        if(isset($cart[$key])){
            unset($cart[$key]);
            Session::put('cart',$cart);

            return $cart;
        }    
    }
}