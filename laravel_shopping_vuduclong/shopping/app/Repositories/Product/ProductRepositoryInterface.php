<?php

namespace App\Repositories\Product;

use App\Repositories\RepositoryInterface;

interface ProductRepositoryInterface extends RepositoryInterface
{
    /**
     * Sync Data Tag
     * 
     * @param $product Product created
     * @param $data Data from request
     * @return void
     */
    public function syncDataTag($product, $data);

    /**
     * FindOrFail = id
     * 
     * @param 
     * @return string
     */
    public function findID($id);

    /**
     * Edit Product
     * 
     * @param $id ID from DB
     * @return string
     */
    public function editProduct($id); 


    /**
     * add to cart
     */
    
    /**
     * View Proflie
     * 
     * @param $user User from DB
     * @param $id ID from DB
     */
    public function view($id);

}

