<?php

namespace App\Repositories\Category;

use App\Categories;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return Categories::class;
    }

    /**
     * Edit 
     * 
     * @param $id ID from DB
     * @return string
     */
    public function Edit($id)
    {
        $category = $this->model->where('slug', $id)->first();

        return $category;
    }
}
?>
