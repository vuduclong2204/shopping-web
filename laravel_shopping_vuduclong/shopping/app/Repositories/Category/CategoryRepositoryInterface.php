<?php

namespace App\Repositories\Category;

use App\Repositories\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * Edit 
     * 
     * @param $id ID from DB
     * @return string
     */
    public function Edit($id); 

}

?>