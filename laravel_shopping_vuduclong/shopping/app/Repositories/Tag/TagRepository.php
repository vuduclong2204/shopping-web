<?php

namespace App\Repositories\Tag;

use App\Repositories\BaseRepository;
use App\Tags;

class TagRepository extends BaseRepository implements TagRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return Tags::class;
    }

    /**
     * Edit 
     * 
     * @param $id ID from DB
     * @return string
     */
    public function edit($id)
    {
        $tag = $this->model->where('slug', $id)->first();

        return $tag;
    }
}

?>