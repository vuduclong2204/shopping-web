<?php

namespace App\Repositories\Tag;

use App\Repositories\RepositoryInterface;

interface TagRepositoryInterface extends RepositoryInterface
{
    /**
     * Edit 
     * 
     * @param $id ID from DB
     * @return string
     */
    public function Edit($id);
}

?>