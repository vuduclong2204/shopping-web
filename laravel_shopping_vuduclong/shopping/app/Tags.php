<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * quan he many to many
     */

    public function blogs()
    {
        return $this->belongsToMany(Blogs::class, 'blogs_tags', 'tag_id', 'blog_id');
    }

       /**
     * quan he many to many
     */

    public function products()
    {
        return $this->belongsToMany(Products::class, 'blogs_tags', 'tag_id', 'blog_id');
    }
}