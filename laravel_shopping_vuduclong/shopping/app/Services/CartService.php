<?php

namespace App\Services;

use App\Repositories\Cart\CartRepository;

class CartService extends BaseService
{
    public function getRepository()
    {
        return CartRepository::class;
    }

    public function addToCart($id)
    {
        $carts = $this->repository->addToCart($id);
        
        return $carts;
    }
}
?>