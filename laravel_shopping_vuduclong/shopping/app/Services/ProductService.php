<?php 

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\Product\ProductRepositoryInterface;

class ProductService extends BaseService
{
    /**
     * Get Repository which need implement in each Repository
     * 
     *  @return void
     */
    public function getRepository()
    {
        return ProductRepositoryInterface::class;
    }

    /**
     * Sync Tag Data Blog
     * 
     * @param $$product Product is created
     * @param $data Data from Request
     * @return App/Repositories/RepositoryProduct/BlogRepositoryInterface
     */
    public function syncTagDataProduct($product, $data)
    {
        return $this->repository->syncDataTag($product, $data);
    }

    /**
     * FindOrFail = id
     * 
     * @param $id Data from DB
     * @return App/Repositories/RepositoryProduct/ProductRepositoryInterface
     */
    public function findID($id)
    {
        return $this->repository->findID($id);
    }

    /**
     * Edit Product
     * 
     * @param
     * @return  App/Repositories/RepositoryProduct/ProductRepositoryInterface
     */
    public function editProduct($id)
    {
        return $this->repository->editProduct($id);
    }

    /**
     * Search blog
     * 
     * @param $data Data from DB
     * @return App/Repositories/RepositoryProduct/ProductRepositoryInterface
     */
    public function searchProduct($data)
    {
        return $this->repository->searchProduct($data);
    }

    /**
     * View Profile
     * 
     * @param $id Data from DB
     * @return App/Repositories/RepositoryUser/UserRepositoryInterface
     */
    public function view($id)
    {
        return $this->repository->view($id);
    }
   

      /**
     * add to cart
     */
    public function addToCart($request)
    {
        return $this->repository->addToCart($request);
    }

    public function updateCart($productId,$request)
    {
        return $this->repository->updateCart($productId,$request);
    }

    public function removeCart($productId)
    {
        return $this->repository->removeCart($productId);
    }

}