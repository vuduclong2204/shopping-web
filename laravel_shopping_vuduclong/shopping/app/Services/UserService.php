<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryInterface;

class UserService extends BaseService
{
    /**
     * Get Repository which need implement in each Repository
     * 
     *  @return void
     */
    public function getRepository()
    {
        return UserRepositoryInterface::class;
    }

     /**
     * Edit 
     * 
     * @param
     * @return  App/Repositories/RepositoryBlog/BlogRepositoryInterface
     */
    public function edit($id)
    {
        return $this->repository->edit($id);
    }

    /**
     * View Profile
     * 
     * @param $id Data from DB
     * @return App/Repositories/RepositoryUser/UserRepositoryInterface
     */
    public function view($id)
    {
        return $this->repository->view($id);
    }

      /**
     * FindOrFail = id
     * 
     * @param $id Data from DB
     * @return App/Repositories/RepositoryUser/UserRepositoryInterface
     */
    public function findUser($id)
    {
        return $this->repository->findUser($id);
    }

}
?>