<?php

namespace App\Services;

abstract class BaseService
{
    protected $repository;

    public function __construct()
    {
        $this->setRepository();
    }

     /**
     * Get Repository which need implement in each Repository
     * 
     *  @return void
     */
    abstract public function getRepository();

    /**
     * Set Repository
     * 
     * @return void
     */
    public function setRepository()
    {
        $this->repository = app()->make(
            $this->getRepository()
        );
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function getByIds($ids)
    {
        return $this->repository->getByIds($ids);
    }

    public function find($id)
    {
        $result = $this->repository->find($id);

        return $result;
    }

    public function create($attributes = [])
    {
        return $this->repository->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        $result = $this->find($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }

        return false;
    }

    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();

            return true;
        }

        return false;
    }
}
?>