<?php

namespace App\Services;

use App\Repositories\Tag\TagRepositoryInterface;

class TagService extends BaseService
{
    
    /**
     * Get Repository which need implement in each Repository
     * 
     *  @return void
     */
    public function getRepository()
    {
        return TagRepositoryInterface::class;
    }

    /**
     * Edit 
     * 
     * @param
     * @return  App/Repositories/RepositoryBlog/BlogRepositoryInterface
     */
    public function Edit($id)
    {
        return $this->repository->Edit($id);
    }
}
?>